import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// import { seedDatabase } from '../seed';
const config = {
    apiKey: "AIzaSyAw_ajqRekYFbxZwPZvIUSs6Hn3hQb_BRM",
    authDomain: "hotel-california-75d53.firebaseapp.com",
    databaseURL: "https://hotel-california-75d53.firebaseio.com",
    projectId: "hotel-california-75d53",
    storageBucket: "hotel-california-75d53.appspot.com",
    messagingSenderId: "803525550881",
    appId: "1:803525550881:web:820f88d5fc2940f63fd460"
};


const firebase = Firebase.initializeApp(config);
// seedDatabase(firebase);
export { firebase };