import React from "react";
import Header from "../components/header/Header";
import AboutContainer from "../containers/AboutContainer";
import ActivityContainer from "../containers/ActivityContainer";
import FooterContainer from "../containers/FooterContainer";
import FormContainer from "../containers/FormContainer";
import HeroContainer from "../containers/HeroContainer";
import MapContainer from "../containers/MapContainer";
import PercentContainer from "../containers/PercentContainer";
import VisitorContainer from "../containers/VisitorContainer";

const Home = () => {
  return (
    <div>
      <Header />
      <HeroContainer />
      <AboutContainer />
      <ActivityContainer />
      <VisitorContainer />
      <PercentContainer />
      <FormContainer />
      <MapContainer />
      <FooterContainer/>
    </div>
  );
};

export default Home;
