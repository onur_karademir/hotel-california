import React from "react";
import { Visitor } from "../components";
import VisitorData from "../data/VisitorData.json";
const VisitorContainer = () => {
  return (
    <Visitor>
      <Visitor.Inner fluid={true}>
        <Visitor.Title>ziyaretçi yorumları</Visitor.Title>
        <Visitor.Text>
          Aşağıda otelimizi ziyaret eden kıymetli misafirlerimizin yorumlarını
          bulabilirsiniz.
        </Visitor.Text>
        <Visitor.Line>
          {VisitorData.map((item) => (
            <Visitor.Card key={item.id}>
              <Visitor.Icon />
              <div>
              <Visitor.Text>{item.subTitle}</Visitor.Text>
              <Visitor.TitleInner>{item.title}</Visitor.TitleInner>
              </div>
            </Visitor.Card>
          ))}
        </Visitor.Line>
        <Visitor.Button>Diğer Yorumlar</Visitor.Button>
      </Visitor.Inner>
    </Visitor>
  );
};

export default VisitorContainer;
