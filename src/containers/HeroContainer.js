import React from "react";
import { Hero } from "../components";

const HeroContainer = () => {
  return (
    <Hero>
        <Hero.Filter></Hero.Filter>
      <Hero.Inner>
        <Hero.Title>
          Hey Merhaba! Hayatındaki en muhteşem tatil deneyimini yaşamaya ve
          gizemlerle dolu bu dünyaya girmeye nedersin?
        </Hero.Title>
        <Hero.Button>Otel Hakkında</Hero.Button>
      </Hero.Inner>
    </Hero>
  );
};

export default HeroContainer;
