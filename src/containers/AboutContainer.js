import React from "react";
import { About } from "../components";

const AboutContainer = () => {
  return (
    <About>
      <About.Inner>
        <About.Title>OTELİMİZ HAKKINDA</About.Title>
        <About.Line>
          <About.Column md="6">
            <About.Text>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop.
            </About.Text>
          </About.Column>
          <About.Column md="6">
            <About.Image
              className="img img-fluid img-responsive"
              src="../images/content-1.jpg"
            ></About.Image>
          </About.Column>
        </About.Line>
      </About.Inner>
    </About>
  );
};

export default AboutContainer;
