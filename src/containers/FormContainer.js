import React,{useState,useContext } from "react";
import { Form } from "../components";
import { FormGroup, Label } from "reactstrap";
import { FirebaseContext } from "../context/firebase";
const FormContainer = () => {
  const { firebase } = useContext(FirebaseContext);
  const db = firebase.firestore();
  db.settings({ timestampsInSnapshots: true });
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [number, setNumber] = useState('');
  const [date, setDate] = useState('');
  const [returndate, setReturndate] = useState('');

  const isInValid = name === '' || email === '' || number === '' || date ==='' || returndate ===''

    function submitHendler(e) {
        e.preventDefault();
        db.collection("user").add({
          date: date,
          email:email,
          name: name,
          number:number,
          returndate:returndate
      }).then(()=>{
        setName('');
        setEmail('');
        setNumber('');
        setDate('');
        setReturndate('');
      })

    }
  return (
    <Form.Inner>
      <Form.Title>iletişim</Form.Title>
      <Form.Text>
        Aşağıdaki formu doldurarak Hotel California'da tatil yapma keyfini
        çıkarabilirsiniz.
      </Form.Text>
      <Form>
        <Form.FormSign onSubmit={submitHendler}>
          <FormGroup className="form-custom">
            <Label className="c-label" htmlFor="text">Ad Soyad</Label>
            <Form.Input type="text" onChange={({target})=>setName(target.value)} value={name} required htmlFor="text" name="text" placeholder="Ad Soyad"></Form.Input>
          </FormGroup>
          <FormGroup className="form-custom">
            <Label className="c-label" for="text">E-Posta</Label>
            <Form.Input type="email" onChange={({target})=>setEmail(target.value)} value={email} required htmlFor="text" name="text" placeholder="E-Posta"></Form.Input>
          </FormGroup>
          <FormGroup className="form-custom">
            <Label className="c-label" htmlFor="text">Telefon</Label>
            <Form.Input onChange={({target})=>setDate(target.value)} value={date} required htmlFor="text" name="text" placeholder="Telefon"></Form.Input>
          </FormGroup>
          <FormGroup className="form-custom">
            <Label className="c-label" htmlFor="text">Giriş Tarihi</Label>
            <Form.Input onChange={({target})=>setNumber(target.value)} value={number} type="date" required htmlFor="text" name="text"></Form.Input>
          </FormGroup>
          <FormGroup className="form-custom">
            <Label className="c-label" htmlFor="text">Çıkış Tarihi</Label>
            <Form.Input onChange={({target})=>setReturndate(target.value)} value={returndate} type="date" required htmlFor="text" name="text"></Form.Input>
          </FormGroup>
          <Form.Button disabled={isInValid}>GÖNDER</Form.Button>
        </Form.FormSign>
      </Form>
    </Form.Inner>
  );
};

export default FormContainer;
