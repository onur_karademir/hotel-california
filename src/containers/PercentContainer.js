import React from "react";
import { Percent } from "../components";
import { Spinner } from "reactstrap";
const PercentContainer = () => {
  return (
    <Percent>
      <Percent.Inner fluid={true}>
        <Percent.Title>Hangi mevsimde hangi aktivite?</Percent.Title>
        <Percent.Text>
          Aşağıdaki tabloda 4 mevsimde hangi aktivitenin hangi orenda
          yapılabileceğini bulabilirsiniz.
        </Percent.Text>
        <Percent.Line>
          <Percent.Column>
            <Spinner
              className="spiner"
              color="primary"
              style={{ width: "8rem", height: "8rem" }}
            />
            <Percent.Progress>%90</Percent.Progress>
            <Percent.TitleInner>Lorem Ipsum is simply</Percent.TitleInner>
              <Percent.Text>Lorem Ipsum is simply</Percent.Text>
          </Percent.Column>
          <Percent.Column>
            <Spinner
              className="spiner"
              color="secondary"
              style={{ width: "8rem", height: "8rem" }}
            />
            <Percent.Progress>%90</Percent.Progress>
            <Percent.TitleInner>Lorem Ipsum is simply</Percent.TitleInner>
              <Percent.Text>Lorem Ipsum is simply</Percent.Text>
          </Percent.Column>
          <Percent.Column>
            <Spinner
              className="spiner"
              color="warning"
              style={{ width: "8rem", height: "8rem" }}
            />
            <Percent.Progress>%90</Percent.Progress>
            <Percent.TitleInner>Lorem Ipsum is simply</Percent.TitleInner>
              <Percent.Text>Lorem Ipsum is simply</Percent.Text>
          </Percent.Column>
          <Percent.Column>
            <Spinner
              className="spiner"
              color="danger"
              style={{ width: "8rem", height: "8rem" }}
            />
            <Percent.Progress>%90</Percent.Progress>
            <Percent.TitleInner>Lorem Ipsum is simply</Percent.TitleInner>
              <Percent.Text>Lorem Ipsum is simply</Percent.Text>
          </Percent.Column>
        </Percent.Line>
      </Percent.Inner>
    </Percent>
  );
};

export default PercentContainer;
