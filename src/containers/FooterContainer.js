import React from "react";
import { Footer } from "../components";
import { FaFacebook,FaTwitterSquare, FaInstagramSquare, FaYoutube } from "react-icons/fa";
const FooterContainer = () => {
  return (
    <Footer>
        <Footer.Text>Death Valley National Park - Harrisburg California</Footer.Text>
      <Footer.IWrapper>
        <FaFacebook className="icons" />
        <FaTwitterSquare className="icons"/>
        <FaInstagramSquare className="icons"/>
        <FaYoutube className="icons"/>
      </Footer.IWrapper>
    </Footer>
  );
};

export default FooterContainer;
