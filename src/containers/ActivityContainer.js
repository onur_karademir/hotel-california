import React from "react";
import { Activity } from "../components";
import Data from "../data/Data.json";
const ActivityContainer = () => {
  return (
    <Activity>
      <Activity.Inner fluid={true}>
        <Activity.Title>aktiviteler</Activity.Title>
        <Activity.Text>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry.
        </Activity.Text>
        <Activity.Line>
          {Data.map((item) => (
            <Activity.Column key={item.id}>
              <Activity.Icon />
              <Activity.TitleInner>{item.title}</Activity.TitleInner>
              <Activity.Text>{item.subTitle}</Activity.Text>
            </Activity.Column>
          ))}
        </Activity.Line>
        <Activity.Line>
          <Activity.Footer>
            <Activity.TitleInner>
              OTELİMİZ HANGİ DÖNEMLERDE AÇIK?
            </Activity.TitleInner>
            <Activity.Text>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum is simply dummy text of the printing and
              typesetting industry.
            </Activity.Text>
          </Activity.Footer>
        </Activity.Line>
      </Activity.Inner>
    </Activity>
  );
};

export default ActivityContainer;
