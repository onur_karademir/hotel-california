import ReactMapGL from "react-map-gl";
import React, { useState } from "react";
const REACT_APP_MAPBOX_TOKEN = `pk.eyJ1Ijoib251cmthcmFkZW1pciIsImEiOiJja2dzYm1yY3AwYTI5MnNwOW0wZ3JzMnN5In0.xe70JXyJxPGeykX4ss7kQA
`
export default function MapContainer() {
  const [viewport, setViewport] = useState({
    latitude: 45.4211,
    longitude: -75.6903,
    width: "90vw",
    height: "100vh",
    zoom: 13,
  });
  return (
    <div className="map">
    <ReactMapGL
      {...viewport}
      mapboxApiAccessToken={REACT_APP_MAPBOX_TOKEN}
      onViewportChange={viewport => {
        setViewport(viewport);
      }}
    ></ReactMapGL>
    </div>
  );
}
