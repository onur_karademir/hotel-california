import React, { useState } from "react";
import { FaHotel } from "react-icons/fa";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
} from "reactstrap";

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar light expand="md" className="navbar-custom-style fixed-top">
        <Container>
          <NavbarBrand href="/" className="my-brand">
            Hotel California
            <FaHotel className="icon"/>
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem className="nav-item">
                <NavLink className="nav-link" href="/">Otelimiz</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink className="nav-link" href="/">Aktiviteler</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink className="nav-link" href="/">Yorumlar</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink className="nav-link" href="/">İletişim</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default Header;
