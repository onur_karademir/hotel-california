export { default as Hero } from "./hero/Hero";
export { default as About } from "./about/About";
export { default as Activity } from "./activity/Activity";
export { default as Percent } from "./percent/Percent";
export { default as Visitor } from "./visitor/Visitor";
export { default as Form } from "./form/Form";
export { default as Footer } from "./footer/Footer";
