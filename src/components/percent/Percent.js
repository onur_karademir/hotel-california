import React from "react";
import {
  Wrapper,
  Title,
  Inner,
  Line,
  Column,
  Image,
  Text,
  TitleInner,
  Icon,
  Progress,
} from "./Percent.El";
const Percent = ({ children, ...restProps }) => {
  return <Wrapper {...restProps}>{children}</Wrapper>;
};
Percent.Title = function PercentTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Percent.Icon = function PercentIcon({ children, ...restProps }) {
  return <Icon {...restProps}>{children}</Icon>;
};
Percent.TitleInner = function PercentTitleInner({ children, ...restProps }) {
  return <TitleInner {...restProps}>{children}</TitleInner>;
};
Percent.Inner = function PercentInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Percent.Line = function PercentLine({ children, ...restProps }) {
  return <Line {...restProps}>{children}</Line>;
};
Percent.Column = function PercentColumn({ children, ...restProps }) {
  return <Column {...restProps}>{children}</Column>;
};
Percent.Image = function PercentImage({ ...restProps }) {
  return <Image {...restProps} />;
};
Percent.Text = function PercentText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};
Percent.Progress = function PercentProgress({ children, ...restProps }) {
  return <Progress {...restProps}>{children}</Progress>;
};

export default Percent;
