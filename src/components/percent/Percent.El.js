import styled from "styled-components/macro";
import { Container, Row } from "reactstrap";
import { GiCutDiamond } from "react-icons/gi";

export const Icon = styled(GiCutDiamond)`
  font-size: 4rem;
`;
export const Wrapper = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  background: #eee;
  text-align: center;
`;

export const Title = styled.h2`
  text-transform: uppercase;
  color: #222;
  text-align: center;
  font-weight: 700 !important;
  transition: all 0.5s ease-in-out;
  text-decoration: underline solid #222;
  text-decoration-style: double;
  padding: 30px 0;
`;
export const TitleInner = styled.h5`
  text-transform: uppercase;
  color: #222;
  text-align: center;
`;
export const Image = styled.img`
  max-width: 100%;
  width: 200px;
  border-radius: 50%;
`;
export const Text = styled.p`
  color: #0e0d0d;
  text-align: center;
  font-size: 15px;
  font-weight: 600;
  @media screen and (max-width: 1000px) {
    font-size: 14px;
  }
`;
export const Progress = styled.h3`
  color: #0e0d0d;
  position:absolute;
  top:13%;
  font-weight: 600;
`;
export const Inner = styled(Container)``;

export const Line = styled(Row)`
  align-items: center;
  justify-content: center;
`;
export const Column = styled.div`
  max-width: 200px;
  width: 100%;
  height: 350px;
  margin: 10px 10px;
  padding: 5px;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  transition: all 0.5s ease-in-out;
  position:relative;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
  }
`;
