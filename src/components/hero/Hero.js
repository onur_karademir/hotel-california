import React from "react";
import { Wrapper, Title, Button, Inner,Filter } from "./Hero.El";
const Hero = ({ bg="true", children, ...restProps }) => {
  return bg ? <Wrapper {...restProps}>{children}</Wrapper>: children;
};

Hero.Title = function HeroTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Hero.Button = function HeroButton({ children, ...restProps }) {
  return <Button {...restProps}>{children}</Button>;
};
Hero.Inner = function HeroInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Hero.Filter = function HeroFilter({ children, ...restProps }) {
  return <Filter {...restProps}>{children}</Filter>;
};
export default Hero;
