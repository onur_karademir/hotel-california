import styled from "styled-components/macro";
import { Container } from "reactstrap";
export const Wrapper = styled.div`
  width: 100%;
  height: 500px;
  background: url(${({ src }) => (src ? `../images/${src}.jpg` : '../images/home.jpg')}) center center / cover
    no-repeat;
    position:relative;
    z-index:1;
`;
export const Inner = styled(Container)`
  padding: 50px 5%;
  display:flex;
  flex-direction:column;
  justify-content:space-evenly;
  align-items:center;
  height: 100%;
  @media screen and (max-width: 769px) {
    padding: 30px 3%;
  }
`;
export const Title = styled.h3`
  color: #ffff;
  text-align:center;
`;
export const Filter = styled.div`
  position:absolute;
  top:0;
  left:0;
  width:100%;background:#231d1d;
  height: 100%;
    opacity: .5;
    z-index: -1;
`;
export const Button = styled.button`
  background-color: #fff;
  color: #222;
  outline: none;
  border: none;
  cursor: pointer;
  padding: 20px 50px;
  white-space: nowrap;
  border-radius: 3px;
  box-shadow: inset 0 0 0 0 #222;
  transition: ease-out 0.4s;
  font-weight: 600;
  font-size: 20px;
  display: block;
  border-bottom:3px solid grey;
  &:hover {
    box-shadow: inset 250px 0 0 0 #222;
    color: #eee;
  }
`;
