import styled from "styled-components/macro";
import { Container, Row } from "reactstrap";
import { GiCutDiamond } from "react-icons/gi";

export const Icon = styled(GiCutDiamond)`
  font-size: 4rem;
`;
export const Wrapper = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  background: #de79de;
  text-align: center;
`;

export const Title = styled.h2`
  text-transform: uppercase;
  color: #222;
  text-align: center;
  font-weight: 700 !important;
  transition: all 0.5s ease-in-out;
  text-decoration: underline solid #222;
  text-decoration-style: double;
  padding: 30px 0;
`;
export const TitleInner = styled.h5`
  text-transform: uppercase;
  color: #222;
  text-align: center;
`;
export const Image = styled.img`
  max-width: 100%;
  width: 200px;
  border-radius: 50%;
`;
export const Text = styled.p`
  color: #0e0d0d;
  text-align: center;
  font-size: 15px;
  font-weight: 600;
  @media screen and (max-width: 1000px) {
    font-size: 14px;
  }
`;
export const Inner = styled(Container)``;
export const Footer = styled.div`
  background-color: #e0a2b9;
  width: 600px;
  padding: 20px 1%;
  border: 8px solid #f95df9;
  margin-left: auto;
  text-align:left !important;
  @media screen and (max-width:1000px) {
   margin: 0 auto !important;
}
`;

export const Line = styled(Row)`
  align-items: center;
  justify-content: center;
`;
export const Column = styled.div`
  max-width: 200px;
  width: 100%;
  height: 350px;
  margin: 10px 10px;
  padding: 5px;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  background-color: #e0a2b9;
  transition: all 0.5s ease-in-out;
  &:hover {
    transform: scale(1.05);
    cursor: pointer;
  }
`;
