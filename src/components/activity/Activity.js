import React from "react";
import {
  Wrapper,
  Title,
  Inner,
  Line,
  Column,
  Image,
  Text,
  TitleInner,
  Icon,
  Footer,
} from "./Activity.El";
const Activity = ({ children, ...restProps }) => {
  return <Wrapper {...restProps}>{children}</Wrapper>;
};

Activity.Title = function ActivityTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Activity.Icon = function ActivityIcon({ children, ...restProps }) {
  return <Icon {...restProps}>{children}</Icon>;
};
Activity.TitleInner = function ActivityTitleInner({ children, ...restProps }) {
  return <TitleInner {...restProps}>{children}</TitleInner>;
};
Activity.Inner = function ActivityInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Activity.Line = function ActivityLine({ children, ...restProps }) {
  return <Line {...restProps}>{children}</Line>;
};
Activity.Column = function ActivityColumn({ children, ...restProps }) {
  return <Column {...restProps}>{children}</Column>;
};
Activity.Image = function ActivityImage({ ...restProps }) {
  return <Image {...restProps} />;
};
Activity.Text = function ActivityText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};
Activity.Footer = function ActivityFooter({ children, ...restProps }) {
  return <Footer {...restProps}>{children}</Footer>;
};

export default Activity;
