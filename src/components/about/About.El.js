import styled from "styled-components/macro";
import { Container, Row, Col } from "reactstrap";

export const Wrapper = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  background: #8ec6e6;
`;

export const Title = styled.h2`
  color: #222;
  text-align: center;
  font-weight: 700 !important;
  transition: all 0.5s ease-in-out;
  text-decoration: underline solid #222;
  text-decoration-style: double;
  padding:30px 0;
`;
export const Image = styled.img`
  border: 5px solid #715a71;
`;
export const Text = styled.p`
  color: #4c4b4b;
  text-align: left;
  font-size: 20px;
  font-weight: 600;
  @media screen and (max-width:1000px) {
    font-size:14px;
}

`;
export const Inner = styled(Container)``;
export const Line = styled(Row)``;
export const Column = styled(Col)`
padding:50px 5%;
@media screen and (max-width:900px) {
  padding:30px 5%;
}
`;
