import React from "react";
import { Wrapper, Title, Inner, Line, Column, Image, Text } from "./About.El";
const About = ({ children, ...restProps }) => {
  return <Wrapper {...restProps}>{children}</Wrapper>;
};

About.Title = function AboutTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
About.Inner = function AboutInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
About.Line = function AboutLine({ children, ...restProps }) {
  return <Line {...restProps}>{children}</Line>;
};
About.Column = function AboutColumn({ children, ...restProps }) {
  return <Column {...restProps}>{children}</Column>;
};
About.Image = function AboutImage({ ...restProps }) {
  return <Image {...restProps} />;
};
About.Text = function AboutText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};

export default About;
