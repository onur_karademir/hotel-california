import React from "react";
import { MyContainer, Column, Inner,IWrapper,Text } from "./Footer.El";
const Footer = ({ children, ...restProps }) => {
  return (
    <MyContainer {...restProps}>
      <Inner>{children}</Inner>
    </MyContainer>
  );
};

Footer.Column = function FooterColumn({ children, ...restProps }) {
  return <Column {...restProps}>{children}</Column>;
};
Footer.Inner = function FooterInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Footer.IWrapper = function FooterIWrapper({ children, ...restProps }) {
  return <IWrapper {...restProps}>{children}</IWrapper>;
};
Footer.Text = function FooterText({ children, ...restProps }) {
    return <Text {...restProps}>{children}</Text>;
  };

export default Footer;
