import styled from "styled-components/macro";
import { Col, Row } from "reactstrap";

export const MyContainer = styled.div`
  border-top: 8px solid #222;
  background-color: #9eecef;
  width: 100%;
  padding: 50px 5%;
  @media screen and (max-width: 600px) {
    padding: 30px 3%;
  }
`;
export const Column = styled(Col)``;

export const Inner = styled(Row)`
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  display: flex;
  flex-direction: column;
`;
export const IWrapper = styled.div`
  max-width: 400px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Text = styled.p`
  color: #4c4b4b;
  text-align: center;
  font-size: 20px;
  font-weight: 600;
  @media screen and (max-width: 1000px) {
    font-size: 14px;
  }
`;
