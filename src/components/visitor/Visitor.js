import React from "react";
import {
  Wrapper,
  Title,
  Inner,
  Line,
  Image,
  Text,
  TitleInner,
  Icon,
  Card,
  Button,
} from "./Visitor.El";
const Visitor = ({ children, ...restProps }) => {
  return <Wrapper {...restProps}>{children}</Wrapper>;
};
Visitor.Title = function VisitorTitle({ children, ...restProps }) {
  return <Title {...restProps}>{children}</Title>;
};
Visitor.Card = function VisitorCard({ children, ...restProps }) {
  return <Card {...restProps}>{children}</Card>;
};
Visitor.Icon = function VisitorIcon({ children, ...restProps }) {
  return <Icon {...restProps}>{children}</Icon>;
};
Visitor.TitleInner = function VisitorTitleInner({ children, ...restProps }) {
  return <TitleInner {...restProps}>{children}</TitleInner>;
};
Visitor.Inner = function VisitorInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Visitor.Line = function VisitorLine({ children, ...restProps }) {
  return <Line {...restProps}>{children}</Line>;
};
Visitor.Image = function VisitorImage({ ...restProps }) {
  return <Image {...restProps} />;
};
Visitor.Text = function VisitorText({ children, ...restProps }) {
  return <Text {...restProps}>{children}</Text>;
};
Visitor.Button = function VisitorButton({ children, ...restProps }) {
  return <Button {...restProps}>{children}</Button>;
};

export default Visitor;
