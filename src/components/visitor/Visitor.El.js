import styled from "styled-components/macro";
import { Container, Row } from "reactstrap";
import { FaUserAlt } from "react-icons/fa";

export const Icon = styled(FaUserAlt)`
  font-size: 4rem;
`;
export const Wrapper = styled.div`
  width: 100%;
  height: auto;
  background: #e6dd9c;
  text-align: center;
`;
export const Card = styled.div`
  background-color: #c3bbbb;
  border-radius:4px;
  box-shadow:0 0 10px;
  width: 400px;
  height: 200px;
  margin: 20px;
  display: flex;
  align-items: center;
  padding: 30px;
  transition: all 0.5s ease-in-out;
  &:hover{
    cursor: pointer;
    transform: scale(1.05);
  }
`;

export const Title = styled.h2`
  text-transform: uppercase;
  color: #222;
  text-align: center;
  font-weight: 700 !important;
  transition: all 0.5s ease-in-out;
  text-decoration: underline solid #222;
  text-decoration-style: double;
  padding: 30px 0;
`;
export const TitleInner = styled.h5`
  text-transform: uppercase;
  color: #222;
  text-align: center;
`;
export const Image = styled.img`
  max-width: 100%;
  width: 200px;
  border-radius: 50%;
`;
export const Text = styled.p`
  color: #0e0d0d;
  text-align: center;
  font-size: 15px;
  font-weight: 600;
  @media screen and (max-width: 1000px) {
    font-size: 14px;
  }
`;
export const Inner = styled(Container)`
padding:20px 20px`;
export const Line = styled(Row)`
  align-items: center;
  justify-content: center;
`;
export const Button = styled.button`
  background-color: #9aa7cc;
  color: #221c54;
  outline: none;
  border: none;
  cursor: pointer;
  padding: 20px 50px;
  white-space: nowrap;
  border-radius: 3px;
  box-shadow: inset 0 0 0 0 #234092;
  transition: ease-out 0.4s;
  font-weight: 600;
  font-size: 20px;
  display: block;
  margin:0 auto;
  border-bottom:3px solid #221c54;
  &:hover {
    box-shadow: inset 250px 0 0 0 #234092;
    color: #eee;
  }
`;
