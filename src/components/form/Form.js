import React from "react";
import { Wrapper, Inner, FormSign, Input,Button,Title,Text } from "./Form.El";
const Form = ({ children, ...restProps }) => {
  return <Wrapper {...restProps}>{children}</Wrapper>;
};

Form.Inner = function FormInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Form.FormSign = function FormFormSign({ children, ...restProps }) {
  return <FormSign {...restProps}>{children}</FormSign>;
};
Form.Input = function FormInput({ children, ...restProps }) {
  return <Input {...restProps}>{children}</Input>;
};

Form.Button = function FormButton({children, ...restProps }) {
  return <Button {...restProps}>{children}</Button>;
};
Form.Text = function FormText({ children, ...restProps }) {
    return <Text {...restProps}>{children}</Text>;
  };
  Form.Title = function FormTitle({ children, ...restProps }) {
    return <Title {...restProps}>{children}</Title>;
  };


export default Form;
