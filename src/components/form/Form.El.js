import styled from "styled-components/macro";
import {Container } from "reactstrap";

export const Wrapper = styled(Container)`
height: auto;
`;
export const Inner = styled.div`
background-color:#2b8bff;
`;
export const FormSign = styled.form`
display:flex ;
flex-direction:row;
flex-wrap:wrap;
align-items:center;
width: 100%;
justify-content:space-evenly;
`;
export const Input = styled.input`
height: 60px;
width:300px;
padding:20px 10px;
box-shadow: 4px 5px 0 0px #cccccc;
outline:none;
border:none;
`;
export const Button = styled.button`
  background-color: #fff;
  color: #222;
  outline: none;
  border: none;
  cursor: pointer;
  padding: 20px 50px;
  white-space: nowrap;
  border-radius: 3px;
  box-shadow: inset 0 0 0 0 #222;
  transition: ease-out 0.4s;
  font-weight: 600;
  font-size: 20px;
  border-bottom:3px solid grey;
  &:hover {
    box-shadow: inset 250px 0 0 0 #222;
    color: #eee;
  }
  &:disabled {
    opacity: 0.5;
    box-shadow: inset 0 0 0 0 #222;
    color: #222;
  }
`;
export const Title = styled.h2`
  text-transform: uppercase;
  color: #222;
  text-align: center;
  font-weight: 700 !important;
  transition: all 0.5s ease-in-out;
  text-decoration: underline solid #222;
  text-decoration-style: double;
  padding: 30px 0;
`;

export const Text = styled.p`
  color: #0e0d0d;
  text-align: center;
  font-size: 15px;
  font-weight: 600;
  @media screen and (max-width: 1000px) {
    font-size: 14px;
  }
`;