import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as ROUTES from "./routes/Routes";
import Home from "./pages/Home";

function App() {
  return (
    <Router>
      <Route exact path={ROUTES.HOME}>
        <Home />
      </Route>
    </Router>
  );
}

export default App;
